package pdfconvert;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;

import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class Main {
    public static void main(String[] args) throws Exception {
        // todo need to change this
        String docPath = "/Users/longka/Downloads/input1.docx";
        // todo need to change this
        String pdfPath = "/Users/longka/Downloads/output1.pdf";

        InputStream in = new FileInputStream(new File(docPath));
        XWPFDocument document = new XWPFDocument(in);
        PdfOptions options = PdfOptions.create();
        OutputStream out = new FileOutputStream(new File(pdfPath));
        PdfConverter.getInstance().convert(document, out, options);

        out.close();
    }
}
