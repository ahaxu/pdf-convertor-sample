### pdf convertor sample

- Clone this repo to your local

```
    git clone https://gitlab.com/ahaxu/pdf-convertor-sample.git
```

- Change dir and build
```
    cd pdf-convertor-sample
```
- Edit hard code of the docx file and output (this is quick and dirty)

```
    // todo need to change this
    String docPath = "/Users/longka/Downloads/input1.docx";
    // todo need to change this
    String pdfPath = "/Users/longka/Downloads/output1.pdf";
```

- Then here you go
```
# build and package with dependencies
mvn clean package assembly:single

# run jar file
java -cp target/info.longka-0.0.1-SNAPSHOT-jar-with-dependencies.jar pdfconvert.Main
```
